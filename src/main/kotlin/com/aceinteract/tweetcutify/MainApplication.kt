package com.aceinteract.tweetcutify

import com.aceinteract.tweetcutify.unsplash.UnsplashClient
import com.sangupta.jerry.http.service.impl.DefaultHttpServiceImpl
import twitter4j.*
import twitter4j.conf.ConfigurationBuilder
import java.io.File
import java.net.URL
import javax.imageio.ImageIO


class MainApplication {

    companion object {

        @JvmStatic
        @Throws(Exception::class)
        fun main(args: Array<String>) {
            println("Hello Bot!")

            val configurationBuilder = ConfigurationBuilder()
            configurationBuilder.setDebugEnabled(true)
                    .setOAuthConsumerKey("7VQGG45f5gXpiLeVCcFERzAFS")
                    .setOAuthConsumerSecret("qP6N7WsIatgE72zYYycV7GuBQH1NaUGN6qTw1OxaL50al04r6o")
                    .setOAuthAccessToken("1043682771208691712-HWSjR2qJ604xEF2SKInVuIwUHGnmfz")
                    .setOAuthAccessTokenSecret("F9Jbh53aPhoYHUiU19uUdE8Om36UwSJjMHA4p2oTouQtu")

            val configuration = configurationBuilder.build()

            val client = UnsplashClient("716591ef20463806b90effd10f1db6ad5f8257102e2dab0e7482d12f8ca6f7ed")
            client.httpService = DefaultHttpServiceImpl()

            val twitterFactory = TwitterFactory(configuration)

            val twitter = twitterFactory.instance

            val listener: StatusListener = object : StatusListener {
                override fun onDeletionNotice(p0: StatusDeletionNotice?) {

                }

                override fun onScrubGeo(p0: Long, p1: Long) {

                }

                override fun onStatus(status: Status) {
                    println(status.user.name + " : " + status.text)

                    val image = client.getRandomPhoto("cute+cat")!!

                    val url = URL(image.urls!!.regular)
                    val img = ImageIO.read(url)
                    val file = File.createTempFile("tempImage", ".tmp")
                    ImageIO.write(img, "jpg", file)

                    val statusReply = StatusUpdate("Hello @" + status.user.screenName + ". Here's a cute cat.").inReplyToStatusId(status.id).media(file)
                    val reply = twitter.updateStatus(statusReply)

                    println("Posted reply " + reply.id + " in response to tweet " + reply.inReplyToStatusId)
                }

                override fun onTrackLimitationNotice(numberOfLimitedStatuses: Int) {
                    println(numberOfLimitedStatuses.toString() + " were limited")
                }

                override fun onException(ex: Exception) {
                    ex.printStackTrace()
                }

                override fun onStallWarning(arg0: StallWarning) {
                    println("Connection stalled")
                }


            }

            val twitterStream = TwitterStreamFactory(configuration).instance
            twitterStream.addListener(listener)

            val filter = FilterQuery()
            filter.track("#tweetcutify")

            twitterStream.filter(filter)
        }

    }

}