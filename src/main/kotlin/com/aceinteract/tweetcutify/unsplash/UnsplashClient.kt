/**
 *
 * unsplash-java-client: Java Client for Unsplash.com API
 * Copyright (c) 2017, Sandeep Gupta
 *
 * https://sangupta.com/projects/unsplash-java-client
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.aceinteract.tweetcutify.unsplash

import com.google.gson.FieldNamingPolicy
import com.sangupta.jerry.constants.HttpHeaderName
import com.sangupta.jerry.http.WebRequest
import com.sangupta.jerry.http.WebResponse
import com.sangupta.jerry.http.service.HttpService
import com.sangupta.jerry.util.AssertUtils
import com.sangupta.jerry.util.GsonUtils
import com.sangupta.jerry.util.UriUtils
import com.sangupta.jerry.util.UrlManipulator
import com.aceinteract.tweetcutify.unsplash.model.UnsplashImage
import com.aceinteract.tweetcutify.unsplash.model.UnsplashPhotoStatistic

class UnsplashClient
/**
 * Constructor
 *
 * @param clientID the client ID to use. Cannot be `null`
 */
(
        /**
         * The clientID
         */
        private val clientID: String) {

    /**
     * Base URL for the API
     */
    protected var baseUrl = "https://api.unsplash.com"

    /**
     * [HttpService] to use
     */
    // Usual accessors follow


    var httpService: HttpService? = null

    /**
     * Get a random photo
     *
     * @return
     */
    fun getRandomPhoto(query: String): UnsplashImage? {
        val url = UriUtils.addWebPaths(this.baseUrl, "/photos/random?query=$query")
        return getJSON(url, UnsplashImage::class.java)
    }

    init {
        if (AssertUtils.isEmpty(clientID)) {
            throw IllegalArgumentException("ClientID cannot be null/empty")
        }
    }

    /**
     * Get photos from the site.
     *
     * @param page
     * @param perPage
     * @param sort
     * @return
     */
    fun getPhotos(page: Int, perPage: Int, sort: UnsplashSort): Array<UnsplashImage>? {
        val url = UriUtils.addWebPaths(this.baseUrl, "/photos?page" + page + "&per_page=" + perPage + "&order_by=" + sort.toString().toLowerCase())
        return getJSON(url, Array<UnsplashImage>::class.java)
    }

    /**
     * Find all curated photos.
     *
     * @param page
     * @param perPage
     * @param sort
     * @return
     */
    fun getCuratedPhotos(page: Int, perPage: Int, sort: UnsplashSort): Array<UnsplashImage>? {
        val url = UriUtils.addWebPaths(this.baseUrl, "/photos/curated?page" + page + "&per_page=" + perPage + "&order_by=" + sort.toString().toLowerCase())
        return getJSON(url, Array<UnsplashImage>::class.java)
    }

    /**
     * Get details of a given photo.
     *
     * @param photoID
     * @return
     */
    fun getPhoto(photoID: String): UnsplashImage? {
        val url = UriUtils.addWebPaths(this.baseUrl, "/photos/$photoID")
        return getJSON(url, UnsplashImage::class.java)
    }

    fun getPhoto(photoID: String, width: Int, height: Int, rect: IntArray): UnsplashImage? {
        var url = UriUtils.addWebPaths(this.baseUrl, "/photos/$photoID")

        var added = false
        if (width > 0) {
            added = true
            url = "$url?w=$width"
        }

        if (height > 0) {
            url += if (added) "&" else "?"
            url = url + "h=" + height
            added = true
        }

        if (AssertUtils.isNotEmpty(rect) || rect.size == 4) {
            url += if (added) "&" else "?"
            url = "rect=" + rect[0] + "," + rect[1] + "," + rect[2] + "," + rect[3]
        }

        return getJSON(url, UnsplashImage::class.java)
    }

    fun getRandomPhoto(collections: String, featured: String, username: String, query: String, width: Int, height: Int, orientation: UnsplashOrientation?, count: Int): Array<UnsplashImage>? {
        val url = UriUtils.addWebPaths(this.baseUrl, "/photos/random")
        val manipulator = UrlManipulator(url)

        if (AssertUtils.isNotEmpty(collections)) {
            manipulator.setQueryParam("collections", collections)
        }

        if (AssertUtils.isNotEmpty(featured)) {
            manipulator.setQueryParam("featured", featured)
        }

        if (AssertUtils.isNotEmpty(username)) {
            manipulator.setQueryParam("username", username)
        }

        if (AssertUtils.isNotEmpty(query)) {
            manipulator.setQueryParam("query", query)
        }

        if (width > 0) {
            manipulator.setQueryParam("w", width.toString())
        }

        if (height > 0) {
            manipulator.setQueryParam("h", height.toString())
        }

        if (orientation != null) {
            manipulator.setQueryParam("orientation", orientation.toString().toLowerCase())
        }

        if (count > 0) {
            manipulator.setQueryParam("count", count.toString())
        }

        // only if count is specified that we will get an array
        if (count <= 0) {
            val image = this.getJSON(url, UnsplashImage::class.java)

            return arrayOf<UnsplashImage>(image!!)
        }

        return this.getJSON(url, Array<UnsplashImage>::class.java)
    }

    /**
     * Get a photo's statistics.
     *
     * @param photoID
     * @return
     */
    fun getPhotoStatistics(photoID: String): UnsplashPhotoStatistic? {
        val url = UriUtils.addWebPaths(this.baseUrl, "/photos/$photoID/statistics")
        return getJSON(url, UnsplashPhotoStatistic::class.java)
    }

    /**
     * Hit the given URL as a HTTP GET request and parse the JSON response
     * into the provided strongly typed object.
     *
     * @param url
     * @param classOfT
     * @return
     */
    private fun <T> getJSON(url: String, classOfT: Class<T>): T? {
        val request = WebRequest.get(url)
        request.addHeader(HttpHeaderName.AUTHORIZATION, "Client-ID " + this.clientID)

        val response = this.httpService!!.executeSilently(request) ?: return null

        if (!response.isSuccess) {
            return null
        }

        val json = response.content
        println(json)
        return if (AssertUtils.isEmpty(json)) {
            null
        } else GsonUtils.getGson(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).fromJson(json, classOfT)

    }

}
