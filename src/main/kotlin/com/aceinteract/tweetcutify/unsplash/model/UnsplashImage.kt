/**
 *
 * unsplash-java-client: Java Client for Unsplash.com API
 * Copyright (c) 2017, Sandeep Gupta
 *
 * https://sangupta.com/projects/unsplash-java-client
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.aceinteract.tweetcutify.unsplash.model

class UnsplashImage {

    var id: String? = null

    var createdAt: String? = null

    var updatedAt: String? = null

    var width: Int = 0

    var height: Int = 0

    var color: String? = null

    var downloads: Int = 0

    var likes: Int = 0

    var views: Int = 0

    var likedByUser: Boolean = false

    var description: String? = null

    var user: UnsplashUser? = null

    var urls: UnsplashUrls? = null

    var categories: List<UnsplashCategory>? = null

    var links: UnsplashLinks? = null

    var exif: UnsplashExif? = null

    var location: UnsplashLocation? = null

    val imageUrl: String?
        get() = if (this.urls == null) {
            null
        } else this.urls!!.raw

    override fun hashCode(): Int {
        return if (this.id == null) {
            0
        } else this.id!!.hashCode()

    }

    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        }

        if (this === other) {
            return true
        }

        if (this.id == null) {
            return false
        }

        if (other is UnsplashImage) {
            val otherImage = other as UnsplashImage?

            return this.id == otherImage!!.id
        }

        return if (other is String) {
            this.id == other
        } else false

    }

    override fun toString(): String {
        return "UnsplashImage: " + this.imageUrl!!
    }

}
