/**
 *
 * unsplash-java-client: Java Client for Unsplash.com API
 * Copyright (c) 2017, Sandeep Gupta
 *
 * https://sangupta.com/projects/unsplash-java-client
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.aceinteract.tweetcutify.unsplash.model

class UnsplashLinks {

    var self: String? = null

    var html: String? = null

    var photos: String? = null

    var likes: String? = null

    var portfolio: String? = null

    var following: String? = null

    var followers: String? = null

    var downlaod: String? = null

    var downloadLocation: String? = null

}
